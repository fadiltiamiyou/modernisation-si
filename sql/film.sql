-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2019-04-03 10:51:39
-- 服务器版本： 10.1.36-MariaDB
-- PHP 版本： 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `produits`
--

-- --------------------------------------------------------

--
-- 表的结构 `film`
--

CREATE TABLE `film` (
  `id_film` int(10) NOT NULL,
  `nom_film` varchar(40) NOT NULL,
  `type_film` varchar(30) NOT NULL,
  `photo_film` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `film`
--

INSERT INTO `film` (`id_film`, `nom_film`, `type_film`, `photo_film`) VALUES
(1, 'lalaland', 'amour', 'images/film/lalaland.jpg'),
(2, 'transformers', 'science fiction', 'images/film/transformers.jpg'),
(3, 'your name', 'amour', 'images/film/your name.jpg'),
(4, 'silent hill', 'horreur', 'images/film/silenthill.jpg'),
(5, 'baisheyuanqi', 'amour', 'images/film/baishe.jpg'),
(6, 'venom', 'science fiction', 'images/film/venom.jpg'),
(7, 'how to train your dragon', 'anime', 'images/film/dragon.jpg');

--
-- 转储表的索引
--

--
-- 表的索引 `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id_film`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `film`
--
ALTER TABLE `film`
  MODIFY `id_film` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
