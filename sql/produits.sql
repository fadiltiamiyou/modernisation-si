-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2019-04-03 10:51:00
-- 服务器版本： 10.1.36-MariaDB
-- PHP 版本： 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `produits`
--

-- --------------------------------------------------------

--
-- 表的结构 `produits`
--

CREATE TABLE `produits` (
  `id_pro` int(10) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Quantite` int(20) NOT NULL,
  `Description` text NOT NULL,
  `Tarif_HT` int(30) NOT NULL,
  `TTC` int(30) NOT NULL,
  `photo` varchar(50) NOT NULL,
  `type` varchar(40) NOT NULL,
  `id_films` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 转存表中的数据 `produits`
--

INSERT INTO `produits` (`id_pro`, `Nom`, `Quantite`, `Description`, `Tarif_HT`, `TTC`, `photo`, `type`, `id_films`) VALUES
(1, 'Affiche  de lalaland', 20, 'affiche de Lalaland,lalaland est un film d\'amour.', 20, 25, 'images/lalaland.jpg', 'affiche', 1),
(2, 'DVD de your name', 100, 'dvd de yourname ', 20, 30, 'images/your name.jpg', 'dvd', 3),
(3, 'Silent hill 4', 100, 'video game ,it is famous', 100, 110, 'images/silenthill.jpg', 'jeu video', 4),
(4, 'Jouet de transformers', 100, 'op', 100, 103, 'images/transformers.jpg', 'jouet', 2),
(5, 'DVD de baisheyuanqi', 300, 'baisheyuanqi est un film chinois d\'amour.', 30, 35, 'images/baishe.jpg', 'dvd', 5),
(6, 'modele de venom', 30, 'modele de venom ', 300, 350, 'images/venom.jpeg', 'modele', 6),
(7, 'Sweat-shirt de dragon', 100, 'ventement ', 50, 55, 'images/how to train your dragon.jpg', 'ventement', 7),
(8, 'Music de lalaland', 300, 'city of stars and so on.', 40, 45, 'images/musiclalaland.jpg', 'music', 1);

--
-- 转储表的索引
--

--
-- 表的索引 `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id_pro`),
  ADD KEY `id_films` (`id_films`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `produits`
--
ALTER TABLE `produits`
  MODIFY `id_pro` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 限制导出的表
--

--
-- 限制表 `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_1` FOREIGN KEY (`id_films`) REFERENCES `film` (`id_film`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
