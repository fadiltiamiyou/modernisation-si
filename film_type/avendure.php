<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="CSS/Index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>FIRST</title>
    <link rel="stylesheet" href="../CSS/filmtype.css">
    <link href="cover.css" rel="stylesheet">
  </head>
  <body class="text-center">
  <?php include("../bdd.php"); ?>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
        <div class="jumbotron text-center bg-dark text-white ">
            <h1><p class="serif">FIRST COMPANY</p></h1>
            <p>Bienvenu ! </p>             
        </div>
        
        <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">
          <!-- Brand -->
          <a class="navbar-brand" href="../Index.php">FIRST</a>
        
          <!-- Toggler/collapsibe Button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse " id="collapsibleNavbar">
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="../Categories.php">CATEGORIES</a>
            </li>
          </ul>

          <div class="collapse navbar-collapse " id="collapsibleNavbar">
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="../page_produits_client/page_produits.php">MAGASIN</a>
            </li>
          </ul>
            
          </div> 
        </nav>
        </div>
    <div class="products-catagories-area">
    <div class="amado-pro-catagory">
    <?php 
            $r = $bdd->query("SELECT * from  film ");
            while($donnees = $r->fetch())
            {
                if($donnees['type_film'] =="aventure")
                {
                    $id=$donnees['id_film'];
                ?>
        <!-- Single Catagory -->
        <div class="single-products-catagory">

            <a href="page_film.php ?id= <?php echo $id ?>">
                <img src="../<?php echo($donnees['photo_film']);?>" >
                <!-- Hover Content -->
                <div class="hover-content">
                    <div class="line"></div>
                    <h4><?php echo($donnees['nom_film']);?></h4>
                </div>
            </a>
        </div>
        <?php
                }
            }
                ?>
    </div>
    </div>

</div>
</body>
</html>