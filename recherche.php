<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>PAGE PRODUITS</title>
        <link rel="stylesheet" href="CSS/PRODUITS.css">
        <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.5">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
  <body>
  <div id=all>
  <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">
<!-- Navbar links -->
<div class="collapse navbar-collapse " id="collapsibleNavbar">
  <ul class="navbar-nav ">
    <li class="nav-item">
      <a class="nav-link" href="Index.php">FIRST</a>
    </li>
  </ul>

<div class="collapse navbar-collapse " id="collapsibleNavbar">
  <ul class="navbar-nav ">
    <li class="nav-item">
      <a class="nav-link" href="Categories.php">CATEGORIES</a>
    </li>
  </ul>
  <ul class="navbar-nav ">
    <li class="nav-item">
      <a class="nav-link" href="page_produits_client/page_produits.php">MAGASIN</a>
    </li>
  </ul>

      </div> 
</nav>
                    <div id=header>
                        <div class="jumbotron text-center bg-dark text-white" id=text>
                            <h1>FIRST COMPANY</h1>
                            <p>Bienvenu ! </p>             
                        </div>      
                    </div>
                    <div id=middle>
                        <div id=menu>
                            <ul id=cate>
                            <li><a href="page_produits_client/page_produits.php">All</a></li>
                            <li><a href="page_produits_client/page_affiche.php">Affiche</a></li>
                            <li><a href="page_produits_client/page_jeu.php">Jeu vidéo</a></li>
                            <li><a href="page_produits_client/page_dvd.php">DVD</a></li>
                            <li><a href="page_produits_client/page_jouet.php">Jouet</a></li>
                            <li><a href="page_produits_client/page_ventement.php">Ventement</a></li>
                            <li><a href="page_produits_client/page_autre.php">Autre</a></li>
                            <li><a href="Categories.php">Licence</a></li>
                            </ul>
                        </div>
                        <div id=list>
                        <?php 
                          $recherche=$_GET['recherche'];
                          include("bdd.php");
                          $r = $bdd->query("SELECT * from produits join film on id_films = id_film");
                           while($donnees=$r->fetch())
                          {
                            if($donnees['nom_film']==$recherche){
                                 ?>
                            <div id=produit>
                                <h1><?php echo $donnees['Nom']?></h1>

                                    <img src="<?php echo($donnees['photo'])?>"class="img-responsive">
                            </div>
                            <div id=description>
                                <h1>INFORMATION</h1>
                                <table>
                                    <tr>
                                        <th>Type: </th>
                                        <td> <?php echo($donnees['type'])?></td>
                                    </tr>
                                    <tr>
                                        <th>Tarif_HT: </th>
                                        <td><?php echo($donnees['Tarif_HT'])?>
                                    </tr>
                                    <tr>
                                        <th>TTC: </th>
                                        <td><?php echo($donnees['TTC'])?>
                                    </tr>
                                    <tr>
                                        <th>Film: </th>
                                        <td><?php echo($donnees['nom_film'])?>
                                    </tr>
                                </table>
                                <div id = des>
                                <div class=titledes><p>Description</p></div>
                                <div class=contdes>
                                <p>
                                <?php echo $donnees['Description'];?>
                                </p>
                                 </div>
                                 </div>
                            </div>
                            <?php
                                  }
                                }   
                            ?>
                        </div>
                    </div>
                    <div id=footer>
                    </div>
                </div>


     
  </body>
  </html>
