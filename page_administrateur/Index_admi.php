<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../CSS/Index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>FIRST</title>

    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
  </head>
  <body class="text-center">
      <?php include("bdd.php");?>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
        <div class="jumbotron text-center bg-dark text-white ">
            <h1><p class="serif">FIRST COMPANY</p></h1>
            <p>Bienvenu Administratuer! </p>             
        </div>
        
        <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">
          <!-- Brand -->
          <a class="navbar-brand" href="Index.php">FIRST</a>
        
          <!-- Toggler/collapsibe Button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>

        <!-- Navbar links -->
        <div class="collapse navbar-collapse " id="collapsibleNavbar">
        <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_adimi_clients.php">CLIENTS</a>
            </li>
          </ul>
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_adimi_employe.php">EMPLOYE</a>
            </li>
          </ul>
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_produits.php">FILIMS</a>
            </li>
          </ul>
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="../Logout.php">DECONNEXION</a>
            </li>
          </ul>
              </nav>
              </div> 
        </nav>
        
  <div class="container">
    <div class="row">
            <div class="col"></div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="../images/xxxx.jpg" alt="First slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="../images/xx.jfif" alt="Second slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="../images/xxx.jpg" alt="Third slide">
                  </div>
                </div>
                          
              </div>
            </div>
              <div class="col"></div>
</div> 
<div class="row">
     
    
       
        <div class="col"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 mid"></div>

 <div id=description>
    <h5>QUE SONT VOS DROITS ?</h5>
    <p>FIRST COMPANY est une société e-commerce qui a ouvert ses portes sur la toile le 25 Septembre 2017. La compagnie a été créée par Sundar Pichai et nous sommes une équipe de passionnés travaillant dans le milieu depuis de nombreuses années. Nous sommes des collectionneurs de produits dérivés et cela nous permet de vous proposer les meilleurs produits possibles des univers jeux vidéo, films et séries, animes et mangas et comics. Tous les articles proviennent des distributeurs officiels européens. Il n’y a aucun produit de contrefaçon (bootleg) sur le site FIRST COMPANY. 
    </p>
    <div class= "entete">
    <img src = "../Images/a.png" style = "height:160px;">
    </div>

<div id=Fondateur>
<p>Sundar Pichai - Fondateur de  FIRST COMPANY</p>
</div>

</div>
</body>
</html>