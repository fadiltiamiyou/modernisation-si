<?php
  session_start();?>
<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>PAGE PRODUITS</title>
      <link rel="stylesheet" href="../CSS/Table.css">
      <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">  
	    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
      <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
      <meta name="generator" content="Jekyll v3.8.5">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      </head>
    <body >
      <?php include("../bdd.php"); ?>
      <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">


      <!-- Navbar links -->
        <div class="collapse navbar-collapse " id="collapsibleNavbar">
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="Index_admi.php">FIRST</a>
            </li>
          </ul>
        </div> 

        <div class="collapse navbar-collapse " id="collapsibleNavbar">
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_produits.php">FILMS</a>
            </li>
            </ul>
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_adimi_clients.php">CLIENTS</a>
            </li>
          </ul>

          </div>
        </div> 
      </nav>
      
        <div id=header>
          <div class="jumbotron text-center bg-dark text-white" id=text>
              <h1>FIRST COMPANY</h1>
              <p>Vos Clients ! </p>             
          </div>
        </div>      
      </div>
      <div id=tab>
        <table border="2" >
          <tr>
            <th>Nom </th>
            <th>Prenom</th>
            <th>Email</th>
            <th>Adresse</th>
          </tr>
          <?php
                // LDAP variables
                // $ldaphost = "195.221.30.7";
                $ldaphost = "10.3.0.152";
                $ldapport = 389;    //ldap server port number
                $base_dn = "OU=". $_SESSION['departement'].", DC=GROUPE-FIRST, DC=COM";
                // $base_dn = "OU=Admin, DC=GROUPE-FIRST, DC=COM";
                $ldapuser = "CN=".$_SESSION['name'].", OU=". $_SESSION['departement'].", DC=GROUPE-FIRST, DC=COM";
                // $ldapuser = "CN=Yinjie ZHAO, OU=Admin, DC=GROUPE-FIRST, DC=COM";
                $ldappass = $_SESSION['password'];
                // $ldappass = "Password0";
                $filter = "(CN=*)";
                $scope = array('sn','givenname','mail','streetaddress');
                // Connecting to ldap
                $connect = ldap_connect($ldaphost,$ldapport);
                ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
                $ldapbind = ldap_bind($connect, $ldapuser, $ldappass);
                    ?>
          <?php 
            $read = ldap_search($connect, $base_dn, $filter,$scope);
            $info = ldap_get_entries($connect, $read);
            for($ligne = 0; $ligne<$info["count"]; $ligne++)
            {
                
                    $data = $info[$ligne][0];
                    echo '<tr><td>'.$info[$ligne][$data][0].'</td>';
                    $data = $info[$ligne][1];
                    echo '<td>'.$info[$ligne][$data][0].'</td>';
                    $data = $info[$ligne][3];
                    echo '<td>'.$info[$ligne][$data][0].'</td>';
                    $data = $info[$ligne][2];
                    echo '<td>'.$info[$ligne][$data][0].'</td></tr>'; 
            };
            ldap_close($connect);
            ?>
        </table>
      </div>
      </div>
      </body>
  </html>