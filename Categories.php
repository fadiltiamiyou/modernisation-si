<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title>Catagories</title>
    <link rel="stylesheet" href="CSS/Categorie.css">
    
  


    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
        <div class="jumbotron text-center bg-dark text-white ">
            <h1><p class="serif">FIRST COMPANY</p></h1>
            <p>Bienvenu ! </p>             
        </div>
        
        <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">
          <!-- Brand -->
          <a class="navbar-brand" href="Index.php">FIRST</a>
        
          <!-- Toggler/collapsibe Button -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse " id="collapsibleNavbar">
          <ul class="navbar-nav ">
            <li class="nav-item">
              <a class="nav-link" href="page_produits_client/page_produits.php">MAGASIN</a>
            </li>
          </ul>
          
        </nav>


<div class="products-catagories-area clearfix">
    <div id="amado-pro-catagory clearfix">

     
        <div class="single-products-catagory clearfix">
            <a href="film_type/action.php">
                <img src="images/a1.jpg" alt="">
        
                <div class="hover-content">
                        <h4>ACTION</h4>
                    </div>                
            </a>
        </div>

   
        <div class="single-products-catagory clearfix">
            <a href="film_type/avendure.php">
                <img src="images/a2.jpg" alt="">
               
                <div class="hover-content">
                    <h4>AVENTURE</h4>
                </div>
            </a>
        </div>

        <div class="single-products-catagory clearfix">
            <a href="film_type/policier.php">
                <img src="images/a3.jpg" alt="">
 
                <div class="hover-content">       
                    <h4>POLICIER</h4>
                </div>
            </a>
        </div>


        <div class="single-products-catagory clearfix">
            <a href="film_type/horreur.php">
                <img src="images/a4.jpg" alt="">

                <div class="hover-content">
                    <h4>HORREUR</h4>
                </div>
            </a>
        </div>


        <div class="single-products-catagory clearfix">
            <a href="film_type/science_fiction.php">
                <img src="images/a5.png" alt="">
             
                <div class="hover-content">    
                   <h4>SCIENCE FICTION</h4>
                </div>
            </a>
        </div>

 
        <div class="single-products-catagory clearfix">
            <a href="film_type/amour.php">
                <img src="images/a6.jpg" alt="">
                
                <div class="hover-content">        
                    <h4>ROMANCE</h4>
                </div>
            </a>
        </div>


        <div class="single-products-catagory clearfix">
            <a href="film_type/comedie.php">
                <img src="images/a7.jpg" alt="">
      
                <div class="hover-content">
                    <h4>COMEDIE</h4>
                </div>
            </a>
        </div>

        <div class="single-products-catagory clearfix">
            <a href="film_type/documentaire.php">
                <img src="images/a8.jpg" alt="">
         
                <div class="hover-content">  
                    <h4>DOCUMENTAIRE</h4>
                </div>
            </a>
        </div>

   
        <div class="single-products-catagory clearfix">
            <a href="film_type/musical.php">
                <img src="images/a9.jpg" alt="">
                
                <div class="hover-content">
                    <h4>MUSICAL</h4>
                </div>
            </a>
        </div>
    </div>
</div>
  
</div>
</body>
</html>