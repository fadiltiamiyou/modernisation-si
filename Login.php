<?php 
  session_start();
  if (isset($_SESSION['name'])&&isset($_SESSION['password'])&&isset($_SESSION['departement'])){
        $name = $_SESSION['name'];
        $password = $_SESSION['password'];
        $departement = $_SESSION['departement'];
  }else{
        $name = null;
        $password = null;
        $departement = null;
  }
  ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
    <link rel="stylesheet" href="CSS/Index.css">
    
</head>
<body>
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
    <div class="inner">
        <div class="jumbotron text-center bg-dark text-white ">
            <h1><p class="serif">FIRST COMPANY</p></h1>
            <p>Bienvenu ! </p>             
        </div>
     <nav class="navbar fixed-top navbar-expand-sm bg-dark navbar-dark">
          <!-- Brand -->
          <a class="navbar-brand" href="Index.php">FIRST</a>
     </nav>
        <div class="container">
                <form action="phpAD/cibleLogin.php" method="post">
                  <div class="form-group">
                    <label for="identifiant">Identifiant</label>
                    <input type="text" class="form-control" id="name" placeholder="Saisir votre nom" name="name">
                  </div>
                  <div class="form-group">
                    <label for="pwd">Mot de passe:</label>
                    <input type="password" class="form-control" id="password" placeholder="Saisir mot de passe " name="password">
                  </div>
                  <div class="form-group">
                    <label for="departement">Département :</label>
                    <select class="form-control" id="departement" name="departement">
                        <option>Commecial</option>
                        <option>Simple Employe</option>
                        <option>Gestionnaire Stock</option>
                        <option>Admin</option>
                      </select>
                    </div> 

                  <div class="form-group form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="checkbox" name="remember"> Se souvenir de moi 
                    </label>
                  </div>
                  <button type="submit" class="btn btn-secondary">Valider</button>
                </form>
              </div>
              
</body>
</html>