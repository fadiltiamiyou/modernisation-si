<?php
$email=$_POST['email'];
$pswd=$_POST['pswd'];

// on teste si nos variables sont définies
if (isset($email) && isset($pswd)) {

	// on vérifie les informations du formulaire, à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
    require_once('param.inc.php');
   
   try
   {
      
      $pdo_option [PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
      $bd = new PDO("mysql:host=".$host.";dbname=".$dbname,$user,$password,$pdo_option);
    }
    catch (Exception $e)
    {
        die ("Erreur: ".$e->getMessage());
    }
    $reponse=$bd->query("SELECT * FROM utilisateur WHERE mail='".$email."' ");
    $utilisateur=$reponse->fetch();
    echo $utilisateur['mail'];
    echo $utilisateur['mot_de_passe'];
    
    if ($utilisateur['mail'] == $email && $utilisateur['mot_de_passe'] == $pswd) {
		// dans ce cas, tout est ok, on peut démarrer notre session

		// on la démarre :)
		session_start ();
		// on enregistre les paramètres de notre visiteur comme variables de session ($login et $pwd) (notez bien que l'on utilise pas le $ pour enregistrer ces variables)
		$_SESSION['login'] = $_POST['email'];
        $_SESSION['pwd'] = $_POST['pswd'];
        $_SESSION['fonction'] = $utilisateur['fonction'];
		// on redirige notre visiteur vers une page de notre section membre
		header ('location: Index.php');
	}
	else {
		// Le visiteur n'a pas été reconnu comme étant membre de notre site. On utilise alors un petit javascript lui signalant ce fait
		echo '<body onLoad="alert(\'Membre non reconnu...\')">';
		// puis on le redirige vers la page d'accueil
		echo '<meta http-equiv="refresh" content="0;URL=Login.php">';
	}
}
else {
	echo 'Les variables du formulaire ne sont pas déclarées.';
}
?>
